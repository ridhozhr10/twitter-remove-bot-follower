import { chromium, firefox, webkit } from "playwright";

type Options = {
  TWITTER_USER: string;
  BLOCK_USER_SCORE: number;
  DEBUG: boolean;
};

const startTheBrowser = async ({
  TWITTER_USER,
  BLOCK_USER_SCORE,
  DEBUG,
}: Options) => {
  let browser;
  try {
    browser = await firefox.launch({
      headless: false,
      timeout: 5 * 60 * 1000,
    });
  } catch (_) {
    try {
      browser = await chromium.launch({
        headless: false,
      });
    } catch (_) {
      try {
        browser = await webkit.launch({
          headless: true,
        });
      } catch (_) {
        throw new Error("FIREFOX, CHROMIUM & WEBKIT IS NOT WORKING");
      }
    }
  }

  const page = await browser.newPage();

  const minScoreToBlock = Number(BLOCK_USER_SCORE);

  await page.goto("https://twitter.com/");
  await page.getByTestId("loginButton").click();
  (await page.waitForSelector("input[name=text]")).fill(TWITTER_USER);
  await page.getByRole("button", { name: "Next" }).click();

  await page.waitForURL(/home$/, { timeout: 5 * 60 * 1000 });
  let botDetected = true;
  while (botDetected) {
    await page.goto(`https://twitter.com/${TWITTER_USER}/followers`);
    await page.waitForSelector(
      `div[aria-label="Timeline: Followers"] div[data-testid="cellInnerDiv"]:nth-child(3)`,
      {
        state: "visible",
        timeout: 15 * 60 * 1000,
      }
    );
    const followersLocator = page.locator(
      `div[aria-label="Timeline: Followers"] div[data-testid="cellInnerDiv"] div[data-testid="UserCell"]:first-child > div`
    );
    const count = await followersLocator.count();
    let imBlocking = false;
    for (let i = 0; i < count; i++) {
      const followerLocator = followersLocator.nth(i);
      const allHtml = await followerLocator.innerHTML();
      const userInfoWrapper = await followerLocator
        .locator("div")
        .elementHandles();

      // const username = ;
      // const bio = await userInfoWrapper[45].innerText();
      let username, bio;

      if (userInfoWrapper[30]) {
        username = await userInfoWrapper[30].innerText();
      }
      if (userInfoWrapper[45]) {
        bio = await userInfoWrapper[45].innerText();
      }
      const possibleBot = {
        botLikeUsername: false,
        noBio: false,
        noProfileImage: false,
      };
      // scoring
      if (username && /\d{5,}$/.test(username)) {
        possibleBot.botLikeUsername = true;
      }
      if (!bio) {
        possibleBot.noBio = true;
      }
      if (
        allHtml.includes(
          "https://abs.twimg.com/sticky/default_profile_images/default_profile_normal.png"
        )
      ) {
        possibleBot.noProfileImage = true;
      }
      const score = Object.values(possibleBot).filter((d) => d).length;
      if (score >= minScoreToBlock) {
        console.log(
          `BOT FOUNDED, REMOVING:`,
          username,
          `BOT SCORE: ${score}`,
          "details",
          possibleBot
        );
        await followerLocator.getByLabel("More").click();
        await page.getByTestId("removeFollower").locator("div").nth(2).click();
        await page.getByTestId("confirmationSheetConfirm").click();
        await page.waitForTimeout(100);
        imBlocking = true;
        break;
      }
    }
    botDetected = imBlocking;
    if (DEBUG) {
      await page.waitForTimeout(2500);
    }
  }

  console.log(`NO MORE BOT, I HOPE`);
  await browser.close();
};

export default startTheBrowser;

const { TWITTER_USER, BLOCK_USER_SCORE, DEBUG } = require("yargs").argv;

startTheBrowser({ TWITTER_USER, BLOCK_USER_SCORE, DEBUG });
