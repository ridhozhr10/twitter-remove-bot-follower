"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var playwright_1 = require("playwright");
var startTheBrowser = function (_a) {
    var TWITTER_USER = _a.TWITTER_USER, BLOCK_USER_SCORE = _a.BLOCK_USER_SCORE, DEBUG = _a.DEBUG;
    return __awaiter(void 0, void 0, void 0, function () {
        var browser, _1, _2, _3, page, minScoreToBlock, botDetected, followersLocator, count, imBlocking, i, followerLocator, allHtml, userInfoWrapper, username, bio, possibleBot, score;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    _b.trys.push([0, 2, , 11]);
                    return [4 /*yield*/, playwright_1.firefox.launch({
                            headless: false,
                            timeout: 5 * 60 * 1000,
                        })];
                case 1:
                    browser = _b.sent();
                    return [3 /*break*/, 11];
                case 2:
                    _1 = _b.sent();
                    _b.label = 3;
                case 3:
                    _b.trys.push([3, 5, , 10]);
                    return [4 /*yield*/, playwright_1.chromium.launch({
                            headless: false,
                        })];
                case 4:
                    browser = _b.sent();
                    return [3 /*break*/, 10];
                case 5:
                    _2 = _b.sent();
                    _b.label = 6;
                case 6:
                    _b.trys.push([6, 8, , 9]);
                    return [4 /*yield*/, playwright_1.webkit.launch({
                            headless: true,
                        })];
                case 7:
                    browser = _b.sent();
                    return [3 /*break*/, 9];
                case 8:
                    _3 = _b.sent();
                    throw new Error("FIREFOX, CHROMIUM & WEBKIT IS NOT WORKING");
                case 9: return [3 /*break*/, 10];
                case 10: return [3 /*break*/, 11];
                case 11: return [4 /*yield*/, browser.newPage()];
                case 12:
                    page = _b.sent();
                    minScoreToBlock = Number(BLOCK_USER_SCORE);
                    return [4 /*yield*/, page.goto("https://twitter.com/")];
                case 13:
                    _b.sent();
                    return [4 /*yield*/, page.getByTestId("loginButton").click()];
                case 14:
                    _b.sent();
                    return [4 /*yield*/, page.waitForSelector("input[name=text]")];
                case 15:
                    (_b.sent()).fill(TWITTER_USER);
                    return [4 /*yield*/, page.getByRole("button", { name: "Next" }).click()];
                case 16:
                    _b.sent();
                    return [4 /*yield*/, page.waitForURL(/home$/, { timeout: 5 * 60 * 1000 })];
                case 17:
                    _b.sent();
                    botDetected = true;
                    _b.label = 18;
                case 18:
                    if (!botDetected) return [3 /*break*/, 37];
                    return [4 /*yield*/, page.goto("https://twitter.com/".concat(TWITTER_USER, "/followers"))];
                case 19:
                    _b.sent();
                    return [4 /*yield*/, page.waitForSelector("div[aria-label=\"Timeline: Followers\"] div[data-testid=\"cellInnerDiv\"]:nth-child(3)", {
                            state: "visible",
                            timeout: 15 * 60 * 1000,
                        })];
                case 20:
                    _b.sent();
                    followersLocator = page.locator("div[aria-label=\"Timeline: Followers\"] div[data-testid=\"cellInnerDiv\"] div[data-testid=\"UserCell\"]:first-child > div");
                    return [4 /*yield*/, followersLocator.count()];
                case 21:
                    count = _b.sent();
                    imBlocking = false;
                    i = 0;
                    _b.label = 22;
                case 22:
                    if (!(i < count)) return [3 /*break*/, 34];
                    followerLocator = followersLocator.nth(i);
                    return [4 /*yield*/, followerLocator.innerHTML()];
                case 23:
                    allHtml = _b.sent();
                    return [4 /*yield*/, followerLocator
                            .locator("div")
                            .elementHandles()];
                case 24:
                    userInfoWrapper = _b.sent();
                    username = void 0, bio = void 0;
                    if (!userInfoWrapper[30]) return [3 /*break*/, 26];
                    return [4 /*yield*/, userInfoWrapper[30].innerText()];
                case 25:
                    username = _b.sent();
                    _b.label = 26;
                case 26:
                    if (!userInfoWrapper[45]) return [3 /*break*/, 28];
                    return [4 /*yield*/, userInfoWrapper[45].innerText()];
                case 27:
                    bio = _b.sent();
                    _b.label = 28;
                case 28:
                    possibleBot = {
                        botLikeUsername: false,
                        noBio: false,
                        noProfileImage: false,
                    };
                    // scoring
                    if (username && /\d{5,}$/.test(username)) {
                        possibleBot.botLikeUsername = true;
                    }
                    if (!bio) {
                        possibleBot.noBio = true;
                    }
                    if (allHtml.includes("https://abs.twimg.com/sticky/default_profile_images/default_profile_normal.png")) {
                        possibleBot.noProfileImage = true;
                    }
                    score = Object.values(possibleBot).filter(function (d) { return d; }).length;
                    if (!(score >= minScoreToBlock)) return [3 /*break*/, 33];
                    console.log("BOT FOUNDED, REMOVING:", username, "BOT SCORE: ".concat(score), "details", possibleBot);
                    return [4 /*yield*/, followerLocator.getByLabel("More").click()];
                case 29:
                    _b.sent();
                    return [4 /*yield*/, page.getByTestId("removeFollower").locator("div").nth(2).click()];
                case 30:
                    _b.sent();
                    return [4 /*yield*/, page.getByTestId("confirmationSheetConfirm").click()];
                case 31:
                    _b.sent();
                    return [4 /*yield*/, page.waitForTimeout(100)];
                case 32:
                    _b.sent();
                    imBlocking = true;
                    return [3 /*break*/, 34];
                case 33:
                    i++;
                    return [3 /*break*/, 22];
                case 34:
                    botDetected = imBlocking;
                    if (!DEBUG) return [3 /*break*/, 36];
                    return [4 /*yield*/, page.waitForTimeout(2500)];
                case 35:
                    _b.sent();
                    _b.label = 36;
                case 36: return [3 /*break*/, 18];
                case 37:
                    console.log("NO MORE BOT, I HOPE");
                    return [4 /*yield*/, browser.close()];
                case 38:
                    _b.sent();
                    return [2 /*return*/];
            }
        });
    });
};
exports.default = startTheBrowser;
var _a = require("yargs").argv, TWITTER_USER = _a.TWITTER_USER, BLOCK_USER_SCORE = _a.BLOCK_USER_SCORE, DEBUG = _a.DEBUG;
startTheBrowser({ TWITTER_USER: TWITTER_USER, BLOCK_USER_SCORE: BLOCK_USER_SCORE, DEBUG: DEBUG });
