/* globals require global */
const _ = require("lodash");

import {
  FlexLayout,
  QApplication,
  QLabel,
  QLineEdit,
  QPlainTextEdit,
  QMainWindow,
  QPushButton,
  QWidget,
  QRadioButton,
  QIcon,
} from "@nodegui/nodegui";
import { exec } from "child_process";

const NUMBERS = _.range(0, 10).map((num: string) => num.toString());
const ALPHABET_LOWER = _.range(97, 123).map((chr: number) =>
  String.fromCharCode(chr)
);
const ALPHABET_UPPER = _.range(65, 91).map((chr: number) =>
  String.fromCharCode(chr)
);
const ALL_POSSIBLE_CHARS = _.range(33, 127).map((chr: number) =>
  String.fromCharCode(chr)
);
const CHARSETS = [
  ALL_POSSIBLE_CHARS,
  [...NUMBERS, ...ALPHABET_LOWER, ...ALPHABET_UPPER],
];

const win = new QMainWindow();
win.setWindowTitle("#JulidFisabilillah - Ababil V.0.1-alpha");
const icon = new QIcon(`:${__dirname}/../assets/logo2.png`);
win.setWindowIcon(icon);
win.resize(400, 300);

// Root view
const rootView = new QWidget();
const rootViewLayout = new FlexLayout();
rootView.setObjectName("rootView");
rootView.setLayout(rootViewLayout);

// Fieldset
const fieldset = new QWidget();
const fieldsetLayout = new FlexLayout();
fieldset.setObjectName("fieldset");
fieldset.setLayout(fieldsetLayout);

// Number characters row
const numCharsRow = new QWidget();
const numCharsRowLayout = new FlexLayout();
numCharsRow.setObjectName("numCharsRow");
numCharsRow.setLayout(numCharsRowLayout);

const twitterUsernameLabel = new QLabel();
twitterUsernameLabel.setText("Username X Kamu (Jangan pakai @): ");
numCharsRowLayout.addWidget(twitterUsernameLabel);

const twitterUsername = new QLineEdit();
twitterUsername.setObjectName("twitterUsername");
numCharsRowLayout.addWidget(twitterUsername);

const radio1 = new QRadioButton();
radio1.setText("Hapus Follower Extreme");
const radio2 = new QRadioButton();
radio2.setText("Hapus Follower Berkemungkinan Bot");
const radio3 = new QRadioButton();
radio3.setText("Hapus Follower Yang Pasti Bot");

// Generated password output
const passOutput = new QPlainTextEdit();
passOutput.setObjectName("passOutput");
passOutput.setReadOnly(true);
passOutput.setWordWrapMode(3);
passOutput.setPlainText("OUTPUT:");

// Button row
const buttonRow = new QWidget();
const buttonRowLayout = new FlexLayout();
buttonRow.setLayout(buttonRowLayout);
buttonRow.setObjectName("buttonRow");

// Buttons
const submitButton = new QPushButton();
submitButton.setText("Jalankan Ababil");
submitButton.setObjectName("submitButton");

const copyButton = new QPushButton();
copyButton.setText("Copy to clipboard");

// Clipboard
const clipboard = QApplication.clipboard();

// Add the widgets to the respective layouts
fieldsetLayout.addWidget(numCharsRow);
fieldsetLayout.addWidget(radio1);
fieldsetLayout.addWidget(radio2);
fieldsetLayout.addWidget(radio3);
rootViewLayout.addWidget(fieldset);
rootViewLayout.addWidget(passOutput);

buttonRowLayout.addWidget(submitButton);
rootViewLayout.addWidget(buttonRow);

// Logic
function getCharSet(includeSpecialCharacters: boolean) {
  return includeSpecialCharacters ? CHARSETS[0] : CHARSETS[1];
}

function generatePassword(passwordLength: string, charSet: string) {
  return _.range(passwordLength)
    .map(() => _.sample(charSet))
    .join("");
}

type Options = {
  TWITTER_USER: string;
  BLOCK_USER_SCORE: number;
  DEBUG: boolean;
};

let browserStarted = false;

// Event handling
submitButton.addEventListener("clicked", () => {
  const config: Options = {
    TWITTER_USER: "",
    BLOCK_USER_SCORE: 0,
    DEBUG: true,
  };
  if (radio1.isChecked()) {
    config.BLOCK_USER_SCORE = 1;
  }
  if (radio2.isChecked()) {
    config.BLOCK_USER_SCORE = 2;
  }
  if (radio3.isChecked()) {
    config.BLOCK_USER_SCORE = 3;
  }
  config.TWITTER_USER = twitterUsername.text();

  if (!browserStarted) {
    browserStarted = true;
    const command = [];
    command.push(`cd ${__dirname} &&`);
    command.push(`node browser.js`);
    command.push(`--DEBUG=${config.DEBUG}`);
    command.push(`--TWITTER_USER=${config.TWITTER_USER}`);
    command.push(`--BLOCK_USER_SCORE=${config.BLOCK_USER_SCORE}`);
    exec(command.join(" "), (err, stdin, stdout) => {
      const output = "OUTPUT: \n" + stdout + "\n" + stdin;
      console.log(output);
      passOutput.setPlainText(output);
      browserStarted = false;
    });
  } else {
    console.log("TAK BOLEH SUDAH START MAA");
  }
});

// Styling
const rootStyleSheet = `
  #rootView {
    padding: 5px;
  }
  #fieldset {
    padding: 10px;
    border: 2px ridge #bdbdbd;
    margin-bottom: 4px;
  }
  #numCharsRow, #buttonRow {
    flex-direction: row;
  }
  #numCharsRow {
    margin-bottom: 5px;
  }
  #numCharsInput {
    width: 40px;
    margin-left: 2px;
  }
  #passOutput {
    height: 85px;
    margin-bottom: 4px;
  }
  #buttonRow{
    margin-bottom: 5px;
  }
  #generateButton {
    width: 120px;
    margin-right: 3px;
  }
  #copyButton {
    width: 120px;
  }
`;

rootView.setStyleSheet(rootStyleSheet);

win.setCentralWidget(rootView);
const command = [];
command.push(`cd ${__dirname} &&`);
command.push(`npm init --y &&`);
command.push(`npm install playwright yargs`);
exec(command.join(" "), (err, stdin, stdout) => {
  win.show();
});

(global as any).win = win;
